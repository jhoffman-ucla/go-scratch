package main

import (
	"fmt"
	"time"
	"sort"
)

type MyStruct struct {
	Id           string
	ArrivalTime time.Time
	Data         []byte
}

func (m *MyStruct) String() string {
	return fmt.Sprintf("%v: %v", m.Id, m.ArrivalTime)
}

func main() {

	// "Slices" are our array type in go
	var my_slice []int
	fmt.Printf("Unitialized slice: %p\n", my_slice)
	fmt.Printf("%v\n", my_slice)

	my_slice = make([]int, 1) // allocate slice with length 1
	my_slice[0] = 10

	my_slice = make([]int, 0, 1) // Allocates a slice with length 0, but capacity of 1

	my_slice = append(my_slice, 234) // Slice of length 1 with capacity 1
	my_slice = append(my_slice, 456) // Will force a reallocation (behind the scenes) of our slice

	//my_slice = make([]int, 0, 10000000) // Allocates a slice with length 0, but capacity of 1

	my_map := make(map[string]*MyStruct)
	
	// fill map with some data
	some_data2 := &MyStruct{
		Id:          "helloworld2",
		ArrivalTime: time.Now().Add(10 * time.Second),
		Data:        []byte{0x2, 0x1},
	}
	my_map[some_data2.Id] = some_data2

	some_data := &MyStruct{
		Id:          "helloworld",
		ArrivalTime: time.Now(),
		Data:        []byte{0x1, 0x2},
	}
	my_map[some_data.Id] = some_data

	fmt.Printf("my_map: %v\n", my_map)

	// Transform our data from a map to a slice
	// Extract our data from our map
	packet_slice := make([]*MyStruct, 0, len(my_map)) // Length is 0, but capacity is "big"
	for _, v := range my_map {
		packet_slice = append(packet_slice, v)
	}

	// Sort our slice
	comparison_func := func(i, j int) bool {
		packet_arrival_1 := packet_slice[i].ArrivalTime
		packet_arrival_2 := packet_slice[j].ArrivalTime
		is_earlier := packet_arrival_1.Before(packet_arrival_2)
		return is_earlier
	}
	sort.Slice(packet_slice, comparison_func)

	fmt.Printf("my_slice: %v\n", packet_slice)
	
}


