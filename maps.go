package main

import (
	"fmt"
)

func OperateOnMyMap(test_map map[string]string) {
	// Can add new keys
	test_map["from function"] = "some data"
	test_map["it's"] = "too hot"

	// Can modify keys that already exist
	test_map["string1"] = "this isn't string2"
	
}

// Maps are like python dictionaries
// maps are just associative arrays

// maps allow you have a data structure that "maps" type1 -> type2
func main() {
	key := "string1"
	val := "string2"
	
	// Declare the map but do not initialize
	var my_map map[string]string
	fmt.Printf("Unitialized map: %p\n", my_map)
	fmt.Printf("%v\n", my_map)
	
	// Initialize the map
	my_map = make(map[string]string)

	// Shorthand for both steps:
	// my_map := make(map[string]string)
	my_map[key] = val
	my_map["test"] = "hey that worked!"

	OperateOnMyMap(my_map)
	
	fmt.Printf("%v\n", my_map)
	fmt.Printf("%p\n", my_map)

	for k, v := range my_map {
		// Handle "unsorted" data
		fmt.Println(k, ": ", v)
	}

}