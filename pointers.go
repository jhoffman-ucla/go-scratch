package main

import (
	"fmt"
)

// A pointer is a variable type that stores the memory address of the
// referenced resource

type my_struct struct {
	struct_int int
	struct_float float32
	my_huge_data []byte
}

// Slices and Maps are passed as pointers
// (also channels, interfaces, and ... maybe some more)
func DoSomeArrayMath(a []int) {
	// if we modify a here, we impact the original array which may or
	// may not be desirable. Under the hood, a pointer is being
	// passed, which means any changes to the "variable" are actually
	// being applied to the original memory space (which is now shared
	// between the original function and this function).
}

func DoSomeMath(a int) int {
	a = a + 20
	return a
}

func DoSomeMathWithPointers(a *int) int {
	
	// Won't work: 
	// a = a + 20 since a is an int POINTER

	// Operate on the *value* stored at "a" rather the address, which is "a"
	(*a) = (*a) + 20

	return *a // *a "extracts" the integer stored at address "a"
}
	
func main() {

	//var my_int int = 234 // 0000 , 0001, 0010,
	//var my_float float32 // 011001 := 1.00000001
	
	//my_int_addr := &my_int // go from variable type to a pointer
	//my_other_int := *my_int_addr // go from pointer back to concrete variable

	// C style
	//int my_int = 234;
	//int * my_int_ptr = &my_int;
	//
	//float * my_float_ptr = (float *) my_int_ptr
	//float my_float = *my_float_ptr

	var start_val, end_val int
	
	fmt.Println("Do math (no pointers:) ")
	start_val = 10
	end_val = DoSomeMath(start_val)
	fmt.Println("Result val: ", end_val)
	fmt.Println("Start  val: ", start_val)

	fmt.Println("Do math (with pointers:) ")
	start_val = 10

	var start_val_ptr *int
	start_val_ptr = &start_val
	end_val = DoSomeMathWithPointers(start_val_ptr)
	fmt.Println("Result val: ", end_val)
	fmt.Println("Start  val: ", start_val)

	fmt.Printf("Start val:     %v\n", start_val)
	fmt.Printf("End val:       %v\n", end_val)
	fmt.Printf("Start val ptr: %v\n", start_val_ptr)

}