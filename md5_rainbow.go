package main

import (
	"time"
	"crypto/md5"
	"fmt"
	"os"
	"encoding/json"
)

func main() {

	table := make(map[string]string)

	start := time.Now()
	for i :=0 ; i < 256; i++ {
		for j:= 0;j < 256; j++ {
			for k:= 0;k < 256; k++ {
				for w:= 0;w < 256; w++ {
					ip := fmt.Sprintf("%d.%d.%d.%d", i, j, k, w)
					hash := fmt.Sprintf("%x", md5.Sum([]byte(ip)))
					fmt.Printf("%s: %s\n", ip, hash)
					table[ip] = hash	
				}
			}
			//fmt.Printf("%d.%d.0.0/16\n", i, j)
		}
	}

	f, _ := os.Create("md5_rainbow.txt")
	err := json.NewEncoder(f).Encode(table)
	if err != nil {
		fmt.Println(err)
	}
	
	fmt.Sprintf("Total elapsed: %v", time.Since(start))
}