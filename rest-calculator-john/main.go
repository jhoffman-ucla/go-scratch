package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"encoding/json"
	"errors"
)

var c Calculator = Calculator{}

type Request struct {
	Operand float64 `json:"operand"`
}

type Response struct {
	Op      string  `json:"op"`
	Operand string  `json:"operand"`
	PrevVal float64 `json:"prevval"`
	NewVal  float64 `json:"currval"`
	Error   string  `json:"error"`
}

func OpHandler(w http.ResponseWriter, r *http.Request) {

	log.Println("OpHandler called")
	
	var err error = nil

	prev_val := c.Equals()

	params := mux.Vars(r)

	op := params["op"]
	operand_str := params["operand"]
	operand, _ := strconv.ParseFloat(operand_str, 64)

	switch op {
	case "add":
		c.Add(operand)
	case "sub":
		c.Sub(operand)
	case "mult":
		c.Mult(operand)
	case "div":
		c.Div(operand)
	case "clear":
		c.Clear()
	default:
		err = errors.New("Invalid operation: " + op)
		log.Println(err)
	}

	var err_encode error 
	if err != nil {
		err_encode = json.NewEncoder(w).Encode(Response{
			Op:      op,
			Operand: operand_str,
			PrevVal: prev_val,
			NewVal:  c.Equals(),
			Error:   err.Error(),
		})
	} else {
		err_encode = json.NewEncoder(w).Encode(Response{
			Op:      op,
			Operand: operand_str,
			PrevVal: prev_val,
			NewVal:  c.Equals(),
			Error:   "",
		})
	}

	

	if err_encode != nil {
		log.Fatal("Failed to encode response json: ", err)
	}
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/{op}/{operand}", OpHandler).Methods(http.MethodGet)
	r.HandleFunc("/{op}", OpHandler).Methods(http.MethodGet)
	http.ListenAndServe(":8080", r)
}
