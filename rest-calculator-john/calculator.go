package main

type Calculator struct {
	Accumulator float64
}

func (c *Calculator) Add(operand float64) {
	c.Accumulator += operand
}

func (c *Calculator) Sub(operand float64) {
	c.Accumulator -= operand
}

func (c *Calculator) Mult(operand float64) {
	c.Accumulator *= operand
}

func (c *Calculator) Div(operand float64) {
	c.Accumulator /= operand
}

func (c *Calculator) Equals() float64 {
	return c.Accumulator
}

func (c *Calculator) Clear() {
	c.Accumulator = 0.0
}

