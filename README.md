# go-scratch

Small code examples for demonstration or testing ideas.

All files should be self-contained and can be invoked with 

``` bash
go run <filename.go>
```