package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"log"
	"io"
	"strconv"
)

var counter int = 0

func NewMessage(command string, data string) *Message {
	counter++
	return &Message{
		Id: counter,
		Command: command,
		Data: data,
	}
}

type Message struct {
	Id      int    `json:"id"`
	Command string `json:"command"`
	Data    string `json:"data"`
}

type Blackboard struct {
	Messages []*Message `json:"Messages"`
}

func (b * Blackboard) WriteMessage(m *Message) {
	b.Messages = append(b.Messages, m)
}

func NewBlackboard() *Blackboard {
	return &Blackboard{
		Messages: make([]*Message, 0, 100),
	}
}


var main_bb *Blackboard
func init() {
	main_bb = NewBlackboard()
}

func BlackboardHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Got BB request from:", r.RemoteAddr)

	err := json.NewEncoder(w).Encode(main_bb);
	if err != nil {
		log.Fatal("You messed up!")
	}
}

func MessageHandler(w http.ResponseWriter, r *http.Request) {
	
	switch r.Method {
	case http.MethodPost:
		m := NewMessage("","")
		err := json.NewDecoder(r.Body).Decode(m)
		if err != nil && err != io.EOF {
			log.Println("ERROR: decode failed: ", err)
			http.Error(w, "Failed to decode message", http.StatusBadRequest)
			return
		}
		log.Println("Got new message: ", m)
		main_bb.WriteMessage(m)
		w.WriteHeader(http.StatusCreated)
		
	case http.MethodGet:
		params := mux.Vars(r)
		id, ok := params["id"]
		if !ok {
			// handle the error
		}

		id_int, _ := strconv.Atoi(id)
		id_int -= 1

		ok = false
		if id_int < len(main_bb.Messages) {
			ok = true
		}
		
		if !ok {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}

		m := main_bb.Messages[id_int]

		err := json.NewEncoder(w).Encode(m)
		if err != nil {
			// handle error
		}

	default:
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}
}

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/blackboard", BlackboardHandler)
	r.HandleFunc("/message", MessageHandler)
	r.HandleFunc("/message/{id}", MessageHandler)
	
	log.Println("Starting server on port 8080...") 
	http.ListenAndServe(":8080", r)

}
