package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"encoding/json"
	"log"
	calc "rest-calculator/pkg/calculator"
)

var calc_gb *calc.Calculator

func init() { 
	calc_gb = calc.NewCalculator()
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/calculator", CalculatorHandler)
	r.HandleFunc("/add", AddHandler)
	r.HandleFunc("/sub", SubtractHandler)
	r.HandleFunc("/div", DivideHandler)
	r.HandleFunc("/mult", MultiplyHandler)
	r.HandleFunc("/equals", EqualsHandler)
	r.HandleFunc("/clear", ClearHandler)

	log.Println("Go to port 8080 for your calculator...")
	http.ListenAndServe(":8080", r)
}

func CalculatorHandler(w http.ResponseWriter, r *http.Request) {
	err := json.NewEncoder(w).Encode(calc_gb)
	if err != nil {
		log.Fatal("ERROR: Failed to encode calculator state: ", err)
	}
}

// I feel like I don't even need to specify the HTTP method here
// because the endpoint specifies the operation... Oh well.
func AddHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		calc_gb.CurrState = calc.NewState(0, 0)

		err := json.NewDecoder(r.Body).Decode(calc_gb.CurrState)
		if err != nil {
			log.Println("Error: JSON decode failed: ", err)
			http.Error(w, "Failed to decode JSON state", http.StatusBadRequest)
			return
		}
		calc_gb.Add()

		log.Println("Processed new add computation: ", calc_gb.CurrState)
		w.WriteHeader(http.StatusCreated)
	} else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func SubtractHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		calc_gb.CurrState = calc.NewState(0, 0)

		err := json.NewDecoder(r.Body).Decode(calc_gb.CurrState)
		if err != nil {
			log.Println("Error: JSON decode failed: ", err)
			http.Error(w, "Failed to decode JSON state", http.StatusBadRequest)
			return
		}
		calc_gb.Subtract()

		log.Println("Processed new subtract computation: ", calc_gb.CurrState)
		w.WriteHeader(http.StatusCreated)
	} else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func DivideHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		calc_gb.CurrState = calc.NewState(0, 0)

		err := json.NewDecoder(r.Body).Decode(calc_gb.CurrState)
		if err != nil {
			log.Println("Error: JSON decode failed: ", err)
			http.Error(w, "Failed to decode JSON state", http.StatusBadRequest)
			return
		}
		calc_gb.Divide()

		log.Println("Processed new divide computation: ", calc_gb.CurrState)
		w.WriteHeader(http.StatusCreated)
	} else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func MultiplyHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		calc_gb.CurrState = calc.NewState(0, 0)

		err := json.NewDecoder(r.Body).Decode(calc_gb.CurrState)
		if err != nil {
			log.Println("Error: JSON decode failed: ", err)
			http.Error(w, "Failed to decode JSON state", http.StatusBadRequest)
			return
		}
		calc_gb.Multiply()

		log.Println("Processed new multiply computation: ", calc_gb.CurrState)
		w.WriteHeader(http.StatusCreated)
	} else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}

func EqualsHandler(w http.ResponseWriter, r *http.Request) {
	curr_answer := calc_gb.Equals()
	err := json.NewEncoder(w).Encode(curr_answer)
	if err != nil {
		log.Fatal("ERROR: Failed to encode answer: ", err)
	}
}

func ClearHandler(w http.ResponseWriter, r *http.Request) {
	calc_gb.Clear()
	log.Println("Calculator cleared by ", r.RemoteAddr)
	err := json.NewEncoder(w).Encode(calc_gb)
	if err != nil {
		log.Fatal("ERROR: Failed to encode calculator after clearing: ", err)
	}
}