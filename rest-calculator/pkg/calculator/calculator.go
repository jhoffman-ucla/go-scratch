package calculator

import "log"

type Calculator struct {
	CurrState *State  `json:"State"`
	cleared   bool    `json:"cleared"`
}

type State struct {
	Argument_1 float64  `json:"arg1"`
	Argument_2 float64  `json:"arg2"`
	Answer     float64  `json:"answer"`
	Operation  string   `json:"operation"`
}

func NewCalculator() *Calculator {
	return &Calculator{
		CurrState: &State{},
		cleared:   true,
	}
}

func NewState(arg1 float64, arg2 float64) *State {
	return &State{
		Argument_1: arg1,
		Argument_2: arg2,
	}
}

func (c *Calculator) Add() {
	c.CurrState.Answer = c.CurrState.Argument_1 + c.CurrState.Argument_2
	c.CurrState.Operation = "add"
	c.cleared = false
}

func (c *Calculator) Subtract() {
	c.CurrState.Answer = c.CurrState.Argument_1 - c.CurrState.Argument_2
	c.CurrState.Operation = "sub"
	c.cleared = false
}

func (c *Calculator) Divide() {
	if c.CurrState.Argument_2 == 0 {
		log.Fatal("ERROR: Cannot divide by 0")
	}
	c.CurrState.Answer = c.CurrState.Argument_1 / c.CurrState.Argument_2
	c.CurrState.Operation = "div"
	c.cleared = false
}

func (c *Calculator) Multiply() {
	c.CurrState.Answer = c.CurrState.Argument_1 * c.CurrState.Argument_2
	c.CurrState.Operation = "mult"
	c.cleared = false
}

func (c *Calculator) Equals() float64 {
	log.Println("Answer of current computation: ", c.CurrState.Answer)
	return c.CurrState.Answer
}

func (c *Calculator) Clear() {
	c.CurrState = &State{}
	c.cleared = true
}