package calculator

import "testing"

func TestCompute(t *testing.T) {
	calc := NewCalculator()

	cases := []struct {
		operation string
		arguments []float64
		expected  float64
	}{
		{
			operation: "add",
			arguments: []float64{2.5, 3.5},
			expected: 2.5+3.5,
		},
		{
			operation: "sub",
			arguments: []float64{3.5, 3.5},
			expected: 3.5-3.5,
		},
		{
			operation: "div",
			arguments: []float64{9.5, 2.5},
			expected: 9.5/2.5,
		},
		{
			operation: "mult",
			arguments: []float64{21.5, 33.5},
			expected: 21.5*33.5,
		},
	}

	for _, c := range cases {
		calc.CurrState = NewState(c.arguments[0], c.arguments[1])

		switch c.operation {
		case "add":
			calc.Add()
		case "sub":
			calc.Subtract()
		case "div":
			calc.Divide()
		case "mult":
			calc.Multiply()
		default:
			t.Error("Failed because unknown math operation.")
		}

		if calc.Equals() != c.expected {
			t.Errorf("Failed because %s operation failed.", c.operation)
		}
	}
}

func TestClear(t *testing.T) {
	calc := NewCalculator()
	
	curr_state := NewState(4.5, 5.5)
	calc.CurrState = curr_state

	calc.Add()
	calc.Subtract()
	calc.Divide()
	calc.Multiply()
	calc.Clear()

	if !calc.cleared {
		t.Error("Failed because clear operation failed.")
	}
}