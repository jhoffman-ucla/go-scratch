# rest-calculator
This is a very sad calculator built with a RESTish API.

## Important information
To run, access localhost:8080 and specify the desired math operator using an appropriate endpoint.

### Supported math operators
- Addition (/add)
- Subtraction (/sub)
- Division (/div)
- Multiplication (/mult)

### Examples
Addition:
```bash
curl -X POST -d '{"arg1":20.2,"arg2":10.3}' localhost:8080/add
```

Division:
```bash
curl -X POST -d '{"arg1":17,"arg2":3}' localhost:8080/div
```

Equals (shows answer):
```bash
curl localhost:8080/equals
```

Clearing the calculator state:
```bash
curl localhost:8080/clear
```